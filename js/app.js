(function( App, undefined ) { 
  
    'use strict';
 
    /*--------------------------
    Start private App members
    Define this blueprints here
    ---------------------------*/
    var _square = function(name, width, height, cb){
        this.name    = name;
        this.width   = width;
        this.height  = height;
        if( typeof cb != 'undefined'){ // prevent errors when callback is not set
           cb(); 
        }
        return false;
    };
    /*--------------------------
    End private App members
    ---------------------------*/
 
    /**
     * @description Main application wrapper under 'mobile' namespace
     * @namespace App.mobile
     */
    App.mobile = {
  
        /**
         * @description Holds something
         * @namespace App.mobile
         */
        buffer1: "string",
 
        /**
         * @description Holds something
         * @namespace App.mobile
         */
        buffer2: [],
 
        /**
         * @description Create squares 
         * @namespace App.mobile
         * @param {String}    <name>      <name of square>
         * @param {Number}    <width>     <width of square>
         * @param {Number}    <height>    <height of square>
         * @param {Function}  <cb>        <callback>
         */
        createSquare: function(name, width, height, cb){
            return new _square(name, width, height, cb);
        },
 
        /**
         * @description The main entry point which controls the lifecycle of the application.
         * @namespace App.mobile
         */
        initApplication: function(){
            // Callback to run after square creation
            var cb = function (){
                console.log('object created');
            };
 
            // Create two squares
            var blueSquare = App.mobile.createSquare('Andrew',25, 25, cb);
            var redSquare  = App.mobile.createSquare('John',27, 27, cb);
 
            console.log(blueSquare);
            console.log(redSquare);
 
        }
    };
 
}( window.App = window.App || {} )); 
  
 
// DOM ready
document.onreadystatechange = function () {
  if (document.readyState == "interactive") {
    App.mobile.initApplication();
  }
};